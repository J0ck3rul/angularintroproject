import { Component, OnInit, Input } from "@angular/core";
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: "app-first",
  templateUrl: "./first.component.html",
  styleUrls: ["./first.component.scss"],
})
export class FirstComponent implements OnInit {
  private title: string = "lalla";

  public form: FormGroup = new FormGroup({
    favFood: new FormControl('', [Validators.required]),
    comment: new FormControl('', [])
  })

  @Input()
  data: string;

  constructor() {}

  ngOnInit() {
    console.log(this.data);
  }

  public clickBtn(): void {
    console.log(this.form.valid);
    console.log(this.form.value.favFood)
  }
}
