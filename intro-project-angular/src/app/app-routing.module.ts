import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstComponent } from './first/first.component';


const routes: Routes = [
  {
    path: 'interactions',
    loadChildren: () => import('./interactions/interactions.module').then(m => m.InteractionsModule)

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
