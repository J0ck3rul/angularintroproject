import { Component, OnInit } from "@angular/core";
import { Subject } from "rxjs";
import { BackEndService } from "../services/back-end.service";

@Component({
  selector: "app-parent",
  templateUrl: "./parent.component.html",
  styleUrls: ["./parent.component.scss"],
})
export class ParentComponent implements OnInit {
  public dataObs: Subject<any> = new Subject();

  public data: string = "data for components";
  public newData: string = "";

  constructor(private backendService: BackEndService) {}

  ngOnInit() {
    this.dataObs.next("1");
    this.backendService.url = "google.ro"
  }

  public updateData(str: string): void {
  }

  public clickMe(): void {
    console.log();
    this.dataObs.next("lalala");
  }
}
