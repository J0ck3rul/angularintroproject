import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BackEndService } from '../services/back-end.service';

@Component({
  selector: 'app-grand-child',
  templateUrl: './grand-child.component.html',
  styleUrls: ['./grand-child.component.scss']
})
export class GrandChildComponent implements OnInit {

  @Input()
  data: any;

  @Output()
  setData: EventEmitter<string> = new EventEmitter();

  constructor(private backendService: BackEndService) { }

  ngOnInit() {
    this.data.subscribe(x => {
      console.log(x);
    })
  }


  public click() {
    const obj = {
      title: 'title',
      body: 'body',
      userId: 1
    }
    console.log(this.backendService.url);

    // this.backendService.post(obj);
  }
}
