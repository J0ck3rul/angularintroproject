import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class BackEndService {
  constructor(private http: HttpClient) {}
  public url: string = '';


  public get(): Observable<any> {
    console.log("URL IS", this.url);

    return this.http.get("https://jsonplaceholder.typicode.com/users");
  }
  public post(obj: any): void {
    const header = new HttpHeaders();
    header.append("Content-type", "application/json");

    this.http
      .post("https://jsonplaceholder.typicode.com/posts", obj, {
        headers: header,
      })
      .subscribe((resp) => {
        console.log(resp);
      });
  }
}
