import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit {

  @Input()
  data: any;

  @Output()
  setData: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public updateData(str: string): void {
    this.setData.emit(str);
  }
}
