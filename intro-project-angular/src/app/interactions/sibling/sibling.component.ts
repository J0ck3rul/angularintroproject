import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sibling',
  templateUrl: './sibling.component.html',
  styleUrls: ['./sibling.component.scss']
})
export class SiblingComponent implements OnInit {

  @Input()
  data: string;

  constructor() { }

  ngOnInit() {
  }

  public printStr() {
    console.log(this.data);

  }

}
