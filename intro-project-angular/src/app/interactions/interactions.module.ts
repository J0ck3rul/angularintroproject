import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InteractionsRoutingModule } from './interactions-routing.module';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { GrandChildComponent } from './grand-child/grand-child.component';
import { SiblingComponent } from './sibling/sibling.component';


@NgModule({
  declarations: [ParentComponent, ChildComponent, GrandChildComponent, SiblingComponent],
  imports: [
    CommonModule,
    InteractionsRoutingModule,
  ]
})
export class InteractionsModule { }
