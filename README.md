# AngularIntroProject

### A intro proiect for angular
* comunicare componente


##  Cuprinde: 
* create new app
* create new component
* reactive forms
* services
* http requests
* routing
* html handling using angular 

## Used materials
* [Http Client Module](https://angular.io/api/common/http)
* [Components interaction](https://angular.io/guide/component-interaction)
* [Angular material - get started](https://material.angular.io/guide/getting-started)
* [Reactive forms](https://angular.io/guide/reactive-forms)
* [Routing](https://angular.io/guide/router)
* [Lazy load module - angular](https://angular.io/guide/lazy-loading-ngmodules)
* [Subject rxjs](https://rxjs-dev.firebaseapp.com/guide/subject)
* [Observable rxjs](https://rxjs-dev.firebaseapp.com/guide/observable)

## Homework project
A proiect with 3 modules:
First module: Welcome page on any routes except the next ones we are going to use
Second module: A create(add) module, where you have a form(with validation )
 * use a service
 * use json placeholder to send the post request [url](https://jsonplaceholder.typicode.com/posts)
 * prompt a success message if request have status "Created"
Third module: A display module, where you will display the data from a get request
 * use a service
 * use json placeholder to send a get request. [url](https://jsonplaceholder.typicode.com/posts)
 * use a angular material table to fill the data from request into page

Exercise for research by yourself
 * create a dynamic search on display module to search in your table
 * create a global service to count how many pages you entered and display the number in header of the app
 